(params) => {
	let name = params.name
	let cmds = {
	bob: ["salades", "tomates", "oignons", "veau", "harissa"]
	sam: ["salade", "tomates", "veau"]
	john: ["salade", "veau"]
	paul: ["salade", "tomate", "oignons", "poulet", "curry", "frites"]
	bill:["salade du couscous vert"]
	}
	return cmds[name]
}